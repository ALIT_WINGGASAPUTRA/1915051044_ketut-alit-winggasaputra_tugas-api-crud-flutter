import 'package:crud/src/ui/formadd/form_add_screen.dart';
import 'package:crud/src/ui/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:crud/MyProfil.dart';
void main() {
  runApp(
    new MaterialApp(

      routes: <String, WidgetBuilder>{

        '/page2' : (BuildContext context) => new MyProfil(),

      },
    ),
  );
}

GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.lightGreenAccent,
        accentColor: Colors.greenAccent,
      ),
      home: Scaffold(

        key: _scaffoldState,
        appBar: new AppBar(
          title: new Text('Flutter CRUD API',style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 40),),
          leading:  new  Icon(Icons.menu,color:Colors.black,),
          centerTitle:  true,
          backgroundColor:Colors.yellowAccent,shadowColor:Colors.lightBlue,
          actions:  <Widget>[
            IconButton(
              icon:  Icon(Icons.person_outline,color:Colors.black),
              onPressed: (){
                Navigator.pushNamed(context, '/page2');
              },
            )
          ],
        ),

        body: HomeScreen(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => FormAddScreen()),
            );
          },
          child: Icon(
            Icons.add,
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
        ),
      ),
    );
  }
}
